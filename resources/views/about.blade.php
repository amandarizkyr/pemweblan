@extends('layouts/main')

@section('title' , 'Profile Admin')


@section('container')
<!-- TESTIMONIAL -->
<section class="testimonial section-padding">
          <div class="container">
               <div class="row">
                    <div class="col-lg-6 col-md-5 col-12">
                        <div class="contact-image" data-aos="fade-up">

                          <img src="images/female-avatar.png" class="img-fluid" alt="website">
                        </div>
                    </div>

                    <div class="col-lg-6 col-md-7 col-8">
                      <h2 class="my-5 pt-3" data-aos="fade-up" data-aos-delay="100">Profile Admin</h2>

                      <div class="quote" data-aos="fade-up" data-aos-delay="200"></div>
                      <div class="card">
                      <div class="card-body">
                      <p data-aos="fade-up" data-aos-delay="100">
                        <strong>Shabrina Amalia Putri</strong>
                        <br>
                        <small>Perempuan</small><br>
                        <small>Jln Semeru Lumajang, Jawa Timur</small>
                      </p>
                  
                    </div>

               </div>
          </div>
     </section>

     <style>
body{
    
    background: linear-gradient(150deg, var(--primary-color) 40%, var(--dark-color) 60%);
    color : white;
}

</style>

@endsection