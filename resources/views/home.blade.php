@extends('layouts/main')

<link rel="stylesheet" type="text/css" href="{!! asset('assets/css/master.css') !!}">

@section('title' , 'Perpustakaan')

@section('container')
<!-- ***** Main Banner Area Start ***** -->
<section class="hero hero-bg d-flex justify-content-center align-items-center">
    <div class="container">
        <div class="row">

            <div class="col-lg-6 col-md-10 col-12 d-flex flex-column justify-content-center align-items-center">
                <div class="hero-text">

                    <h1 class="text-white" data-aos="fade-up">Welcome Admin!</h1>

                    <a href="{{url('/books')}}" class="custom-btn btn-bg btn mt-3" data-aos="fade-up"
                        data-aos-delay="100">Enjoy your work!</a>

                </div>
            </div>

            <div class="col-lg-6 col-12">
                <div class="hero-image" data-aos="fade-up" data-aos-delay="300">

                    <img src="images/working-girl.png" class="img-fluid" alt="working girl">
                </div>
            </div>

        </div>
    </div>
</section>

<!-- ***** Main Banner Area End ***** -->


<!-- PROJECT -->
<section class="project section-padding" id="project">
    <div class="container-fluid">
        <div class="row">

            <div class="col-lg-12 col-12">

                <h2 class="mb-5 text-center" data-aos="fade-up">
                    Our
                    <strong>Team</strong>
                </h2>

                <div class="owl-carousel owl-theme" id="project-slide">
                    <div class="item project-wrapper" data-aos="fade-up" data-aos-delay="100">
                        <img src="images/project/sima.png" class="img-fluid" alt="project image">

                        <div class="project-info">


                            <h3>
                                <a href="project-detail.html">
                                    <span>SIMA</span>
                                    <i class="fa fa-angle-right project-icon"></i>
                                </a>
                            </h3>
                        </div>
                    </div>

                    <div class="item project-wrapper" data-aos="fade-up">
                        <img src="images/project/shab.jpg" class="img-fluid" alt="project image">

                        <div class="project-info">


                            <h3>
                                <a href="project-detail.html">
                                    <span>SHABRINA</span>
                                    <i class="fa fa-angle-right project-icon"></i>
                                </a>
                            </h3>
                        </div>
                    </div>

                    <div class="item project-wrapper" data-aos="fade-up">
                        <img src="images/project/man.jpg" class="img-fluid" alt="project image">

                        <div class="project-info">


                            <h3>
                                <a href="project-detail.html">
                                    <span>Amanda</span>
                                    <i class="fa fa-angle-right project-icon"></i>
                                </a>
                            </h3>
                        </div>
                    </div>

                    <div class="item project-wrapper" data-aos="fade-up">
                        <img src="images/project/reza.png" class="img-fluid" alt="project image">

                        <div class="project-info">


                            <h3>
                                <a href="project-detail.html">
                                    <span>Reza</span>
                                    <i class="fa fa-angle-right project-icon"></i>
                                </a>
                            </h3>
                        </div>
                    </div>


                </div>
            </div>

        </div>
    </div>
</section>
@endsection