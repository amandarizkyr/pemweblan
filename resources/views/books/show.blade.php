@extends('layouts/main')

@section('title' , 'Detail Buku')


@section('container')
<div class="container">
    <div class="row">
        <div class="col-6">
            <h1 class="mt-3">Detail Buku</h1>
            <div class="card">
                <div class="card-body">
                    <h5 class="card-title">{{$book->judul}}</h5>
                    <td><img class="img2" src="/image/{{$book->gambar}}"></td>
                    <br>
                    <br>
                    <h6 class="card-subtitle mb-2 text-muted"><span class="span"> Penulis : </span> {{$book->penulis}}
                    </h6>
                    <p class="card-subtitle mb-2 text-muted"><span class="span"> Penerbit : </span> {{$book->penerbit}}
                    </p>
                    <p class="card-subtitle mb-2 text-muted"><span class="span"> Tahun : </span>{{$book->tahun}}</p>
                    <p class="card-subtitle mb-2 text-muted"><span class="span"> Sinopsis : </span>{{$book->sinopsis}}
                    </p>
                    <a href="{{$book->id}}/edit" class="btn btn-primary">Edit</a>
                    <form action="{{$book->id}}" method=post class="d-inline">
                        @method('delete')
                        @csrf
                        <button type="submit" class="btn btn-danger">Delete</button>
                    </form>
                    <a href="/books" class="btn btn-success">Kembali</a>
                </div>
            </div>


        </div>
    </div>
</div>
<style>
body {

    background: linear-gradient(160deg, var(--primary-color) 40%, var(--dark-color) 60%);
    color: white;
}

.fa {
    color: white;
}

.list {
    color: gray;
}

.list1 {
    color: yellow;
}

.card-title {
    color: gray;
}


.img {
    width: 200px !important;
    height: 200px !important;
}

.img2 {
    width: 300px !important;
    height: 300px !important;
}

.span {
    color: black !important;
    ;
}
</style>
@endsection